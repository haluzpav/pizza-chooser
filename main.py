import bs4
import pprint


def parse_miki(file):
    pizzas = {}

    with open(file, encoding="utf8") as f:
        soup = bs4.BeautifulSoup(f, features='html.parser')

    d: bs4.Tag = soup.find('div', attrs={'class': 'entry-content'})

    p: bs4.Tag
    for p in d.find_all('p'):
        i: bs4.Tag = p.find('img')
        if i is None:
            continue
        name = p.find_previous('p').text
        name = name.strip()
        ingrs = p.find(text=True, recursive=False)
        ingrs = ingrs.split(', ')
        pizzas[name] = set(ingrs)

    return pizzas


def main():
    file = r'Nabídka pizzy – Pizza Miki.htm'
    forbidden_names = {
        'Vegetariana',
        'Margherita',
        'Napoli'
    }
    forbidden_ingrs = {
        'smetana',
        'olivy',
        'žampióny',
        'tuňák',
        'ananas',
        'vejce',
        'slanina',
        'uzený sýr',
        'chilli'
    }

    pizzas_orig = parse_miki(file)
    pizzas = pizzas_orig
    pizzas = {n: i for n, i in pizzas.items() if n not in forbidden_names}
    pizzas = {n: i for n, i in pizzas.items() if len(i.intersection(forbidden_ingrs)) == 0}

    print(f'Showing {len(pizzas)} / {len(pizzas_orig)}')
    pp = pprint.PrettyPrinter(compact=True)
    pp.pprint(pizzas)


if __name__ == '__main__':
    main()

